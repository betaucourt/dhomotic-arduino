# DHOMOTIC
### Découvrons la domotique ensemble !
---
#### Contenu

Dans le dossier de ce repository, vous trouverez les fichiers suivants:

- Un dossier ARDUINO contenant le code qui servira à programmer l'ARDUINO
- Un dossier WIFI8266 contenant le code qui servira à programmer le module WIFI
- Un APK qui vous permettra d'installer l'application mobile Dhomotic sur votre smartphone



Retrouvez dans votre pack **Dhomotic** un tutoriel détaillé et illustré qui vous guidera pas à pas 
pour installer l'application mobile sur votre téléphone, ainsi que toutes les étapes de montage et d'installation.
___

