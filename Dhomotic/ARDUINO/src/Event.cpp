#include "Credential.h"
#include "Event.h"

char masterSent;
//Fonction utilisé lorsque le master envoie des données. Lecture uniquement
void receiveEvent(int length) {
  String receiveData;
  while (Wire.available()) { // Available donne le nombre de message disponible 
    masterSent = Wire.read(); // Wire.read() diminue le nbr de message dans la stack de 1
    receiveData += masterSent;
  }
  if (receiveData.equals("true")){
    digitalWrite(ledPin, HIGH);
  }
  else if (receiveData.equals("false")){
    digitalWrite(ledPin, LOW);
  }
  if (receiveData.equals("day")) {
    digitalWrite(lightLed, LOW);
  }
  else if (receiveData.equals("night")) {
    digitalWrite(lightLed, HIGH);
  }
}

//Fonction utilisé lorsque le maître à besoin de donnée du slave.
void Event::setupEvent() {
  Wire.begin(8);// Adresse du master
  Wire.onReceive(receiveEvent); // Fonction utilisé uniquement côté slave
}