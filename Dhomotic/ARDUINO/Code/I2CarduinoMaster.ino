#include <Arduino.h>
#include <Wire.h>

//master
void setup() {
  Serial.begin(115200);
  Wire.begin();
  Serial.println("Start");
}

void loop() {
 Wire.beginTransmission(8);
 Wire.write("Hello from master"); 
 Wire.endTransmission();  
 
 Wire.requestFrom(8,20); // Canal 8 2O byte
 while (Wire.available()) {
   char c = Wire.read();
   Serial.println(c);
 }
 
 Serial.println();
 delay(1000);
}