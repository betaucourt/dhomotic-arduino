#include <Arduino.h>
#include <Wire.h>

//Slave
void receiveEvent(int byte) {
  while (0 < Wire.available()) // Available donne le nombre de message disponible 
  {
    char x = Wire.read(); // Read diminue le nombre de message de 1
    Serial.print(x);
  }
  Serial.println();
}

void requestEvent(){
  Wire.write("Hey from slave");
}

void setup() {
  Serial.begin(115200);
  Wire.begin(8); // Ecoute sur le canal 8
  Wire.onReceive(receiveEvent);
  Wire.onRequest(requestEvent);
  Serial.println("Arduino start");
}

void loop() {
  delay(100);
}