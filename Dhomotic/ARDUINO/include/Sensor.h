#ifndef SENSOR_H
#define SENSOR_H

#include <Adafruit_Sensor.h>
#include <DHT.h>

#define DHT_PIN 7
#define DHT_TYPE DHT11

typedef struct SensorData
{
  String humi;
  String temp;
}SensorData;

class Sensor {
  public:
    SensorData data;
    void setupSensor();
    void loopsensor();
};

#endif