#ifndef EVENT_H
#define EVENT_H

#include <Wire.h>
#define ledPin 13
#define lightLed 12

static void receiveEvent(int length);

class Event {
  public:
    void setupEvent();  
};

#endif