#ifndef PHOTORESISTOR_H
#define PHOTORESISTOR_H

#include <Arduino.h>

const int sensorPin = A1;

typedef struct PhotoData
{
  String photo;
}PhotoData;

class Photoresistor {
  public:
    PhotoData data;
    void photoLoop();
};

#endif