#ifndef MQTT_H
#define MQTT_H

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include <stdlib.h>

void callback(char* topic, byte* payload, unsigned int length);
class Mqtt {
  public :
    void mqttConnect();
    void setup_mqtt();
    void setupI2C();
    void loopI2C();
    String getValue(String data, char separator, int index);
};

#endif