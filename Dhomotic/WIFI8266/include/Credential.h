#ifndef CREDENTIAL_H
#define CREDENTIAL_H

#define MQTT_BROKER "maqiatto.com"
#define MQTT_BROKER_PORT 1883
#define MQTT_USERNAME "à remplacer" // adresse email
#define MQTT_KEY "à remplacer"      // motdepasse

class Credential
{
public:
  // infos wifi
  const char *ssid = "à remplacer";     // nom de votre wifi
  const char *password = "à remplacer"; // mot de passe wifi

  // infos mqtt
  const char *led = "à remplacer";         // topic correspondant à la led
  const char *ledStatus = "à remplacer";   // topic correspondant à l'allumage automatique de la LED en fonction de la luminosité
  const char *humidity = "à remplacer";    // topic correspondant à l'humidité
  const char *temperature = "à remplacer"; // topic correspondant à la température
  const char *luminosity = "à remplacer";  // topic correspondant à la luminosité
};
#endif