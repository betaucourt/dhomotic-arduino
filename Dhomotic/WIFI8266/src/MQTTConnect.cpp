#include "Credential.h"
#include "MQTTConnect.h"

WiFiClient espClient;
PubSubClient client(espClient);
Credential credentialConnect;
char masterReceive;
String iotId = MQTT_USERNAME;

void Mqtt::mqttConnect() {
  while (!client.connected()) {
    Serial.println("Connection au serveur ...");
    if (client.connect("ESPCLIENT", MQTT_USERNAME, MQTT_KEY)) {
      Serial.println("MQTT connecté");
    }
    else {
      Serial.println("Echec, code erreur = ");
      Serial.println(client.state());
      Serial.println("nouvel essai dans 2s");
      delay(2000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  String payloadStr;
  for (unsigned int i = 0; i < length; i++) {
    payloadStr += (char)payload[i];
  }
  if (String(topic) == credentialConnect.ledStatus)
  {
    if(payloadStr == "true"){
      String ledvalue = "true";
      String ledStatus = "{ \"iotId\" : \"" + iotId + "\", \"value\" : \"" + ledvalue + "\"}";
      client.publish(credentialConnect.led, ledStatus.c_str());
    }
    else if(payloadStr == "false"){
      String ledvalue = "false";
      String ledStatus = "{ \"iotId\" : \"" + iotId + "\", \"value\" : \"" + ledvalue + "\"}";
      client.publish(credentialConnect.led, ledStatus.c_str());
    }
  }
  if (String(topic) == credentialConnect.lampStatus)
  {
    if(payloadStr == "day"){
      String greenLedValue = "false";
      String greenLedFormat = "{ \"iotId\" : \"" + iotId + "\", \"value\" : \"" + greenLedValue + "\"}";
      client.publish(credentialConnect.lamp, greenLedFormat.c_str());
    }
    else if(payloadStr == "night"){
      String lampValue = "true";
      String lampStatus = "{ \"iotId\" : \"" + iotId + "\", \"value\" : \"" + lampValue + "\"}";
      client.publish(credentialConnect.lamp, lampStatus.c_str());
    }
  }
  client.loop();
  Wire.beginTransmission(8);
  Wire.write(payloadStr.c_str());
  Wire.endTransmission();
}

void Mqtt::setup_mqtt() {
  client.setServer(MQTT_BROKER, MQTT_BROKER_PORT);
  mqttConnect();
  client.setCallback(callback);
}

void Mqtt::setupI2C() {
  Wire.begin(); //Maître pas besoin de spécifier d'adresse
  Serial.println("I2C: ESP start");
}

void Mqtt::loopI2C() {
  String receiveData;
  Wire.requestFrom(8,16); // Canal 8 2O byte
  while (Wire.available()) {
   char c = Wire.read();
   receiveData += c;
  }
  String humi = getValue(receiveData, ';', 0);
  String temp = getValue(receiveData, ';', 1);
  String light = getValue(receiveData, ';', 2);
  String formatTemp = "{ \"iotId\" : \"" + iotId + "\", \"value\" : \"" + temp + "\"}";
  String formathumi = "{ \"iotId\" : \"" + iotId + "\", \"value\" : \"" + humi + "\"}";

  int result = atoi(light.c_str());
  String formatLight = "{ \"iotId\" : \"" + iotId + "\", \"value\" : \"" + result + "\"}";

  client.loop();
  client.publish(credentialConnect.humidity, formathumi.c_str());
  client.publish(credentialConnect.temperature, formatTemp.c_str());
  client.loop();
  client.publish(credentialConnect.luminosity, formatLight.c_str());
  client.loop();

  client.subscribe(credentialConnect.ledStatus);
  client.subscribe(credentialConnect.lampStatus);
  client.loop();
}

String Mqtt::getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;

  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }
  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}