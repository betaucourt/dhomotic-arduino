#include "Credential.h"
#include "Wifi.h"

ESP8266WiFiMulti WiFiMulti ;
Credential credentialWifi;

void Wifi::setup_wifi() {
  WiFiMulti.addAP(credentialWifi.ssid, credentialWifi.password);  
  while ( WiFiMulti.run() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }
  Serial.println("");
  Serial.println("WiFi connecté");
  Serial.print("MAC : ");
  Serial.println(WiFi.macAddress());
  Serial.print("Adresse IP : ");
  Serial.println(WiFi.localIP());
}